# dolphin-emu-rpi4


## Rev 1.1

See 5.14 kernel linux, and rootfs, based on debian nightly.

arm64

Double Dash works. 



## Rev 1.2 

arm64 sid, i.e. bookworm.

1.) debootstrap

2.) shadow

3.) fstab with mmcblk0p1, modify sid to bookworm into sources.list.

4.) copy modules and boot. 

5.) edit /mnt/cmdline.txt to change part uuid to /dev/mmcblk0p2 

6.) run 

1656757979-1-linux-modules-arm64-5.14.0-4-arm64-debian-sid-working-noGL-noGL-v1.2-bof.tar.gz

![](media/1656756106-1-dolphin-emu-on-rpi4.png)

Rather slow on game disney cars



## Rev 1.3

Here the installation / code for gamecube:

1. Raspios 64bits for PI 4:

Download:
https://downloads.raspberrypi.org/raspios_arm64/images/raspios_arm64-2022-09-26/2022-09-22-raspios-bullseye-arm64.img.xz

apt-get install xz-utils

xzcat 2022-09-22-raspios-bullseye-arm64.img.xz > /dev/...

Installation


````
apt-get update 
apt-get install --no-install-recommends ca-certificates qtbase5-dev qtbase5-private-dev git cmake make gcc g++ pkg-config libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libxi-dev libxrandr-dev libudev-dev libevdev-dev libsfml-dev libminiupnpc-dev libmbedtls-dev libcurl4-openssl-dev libhidapi-dev libsystemd-dev libbluetooth-dev libasound2-dev libpulse-dev libpugixml-dev libbz2-dev libzstd-dev liblzo2-dev libpng-dev libusb-1.0-0-dev gettext


git clone https://github.com/dolphin-emu/dolphin.git

cd dolphin 

//next instruction is new
// git submodule update --init     
git submodule update --init --recursive


mkdir build && cd build
cmake ..
make
make install 
````

